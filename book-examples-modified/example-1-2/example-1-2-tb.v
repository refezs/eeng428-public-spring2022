// top-level testbench module for the seven segment decoder
module binaryToESegSim;

  wire eSeg, p1, p2, p3, p4;
  reg A, B, C, D;

  // design under test (DUT): seven segment display driver for segment e
  nand #1
    g1 (p1, C, ~D),
    g2 (p2, A, B),
    g3 (p3, ~B, ~D),
    g4 (p4, A, C),
    g5 (eSeg, p1, p2, p3, p4);

  initial
  begin
    $monitor ($time,,,
    "A = %b B = %b C = %b D = %b, eSeg = %b",
    A, B, C, D, eSeg);

    // simulate input changes to drive the design under test
    #10 A = 0; B = 0; C = 0; D = 0;
    #10 D = 1;
    #10 C = 1; D = 0;
    #10 $finish;
  end

  initial
  begin
    // dump signal traces into a VCD file
    $dumpfile("example-1-2-waveform.vcd");
    $dumpvars(0,binaryToESegSim);
  end

endmodule
