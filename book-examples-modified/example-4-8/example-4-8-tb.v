`define READ 0
`define WRITE 1

module sbus;
  parameter tClock = 20;

  reg clock;
  reg [15:0] m [0:31];
  reg [15:0] data;

  reg rwLine;
  reg [4:0] addressLines;
  reg [15:0] dataLines;

  initial
  begin
    // Initialize m register array with data from memory.data file,
    // interpret data in memory.data as hexadecimal values
    // need to make sure there are 32 16-bit values
    $readmemh ("memory.data", m);
    clock = 0;
    $monitor ("rw=%d, data=0x%h, addr=0x%h at time %d",
              rwLine, dataLines, addressLines, $time);
  end

  initial
  begin
    // dump signal traces into a VCD file
    $dumpfile("example-4-8-waveform.vcd");
    $dumpvars(0,sbus);
  end

  initial
  begin
    #10
    wiggleBusLines(`READ, 2, data);
    wiggleBusLines(`READ, 3, data);
    data = 5;
    wiggleBusLines(`WRITE, 2, data);
    data = 7;
    wiggleBusLines(`WRITE, 3, data);
    wiggleBusLines(`READ, 2, data);
    wiggleBusLines(`READ, 3, data);
    // Verilog does not have "final" statement, this could be
    // put in a "final" statement in System Verilog
    #10
      $writememh ("memory.data.out", m);
    #10
    $finish;
  end

  always
    #tClock clock = ~clock;

  task wiggleBusLines
  ( input readWrite,
    input [5:0] addr,
    inout [15:0] data
  );
  begin
    rwLine <= readWrite;
    if (readWrite)
    begin
      addressLines <= addr;
      dataLines <= data;
    end
    else
    begin
      addressLines <= addr;
      @ (negedge clock);
    end
    @ (negedge clock);
    if (~readWrite)
      data <= dataLines;
  end
  endtask

  always
  begin
    @ (negedge clock);
    if (~rwLine)
    begin
      dataLines <= m[addressLines];
      @ (negedge clock);
    end
    else
      m[addressLines] <= dataLines;
  end
endmodule
