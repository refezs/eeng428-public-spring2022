The FIFOs have been purosefully broken. The code is further slightly obfuscated using the tool
that can be found at:

https://github.com/chipsalliance/verible/tree/master/verilog/tools/obfuscator

Binaries can be downlaoded from:

https://github.com/chipsalliance/verible/releases

Make sure to put the "verible-v*/bin/" in the PATH so the tools can be found.

Simple obfuscation using:

verible-verilog-obfuscate --preserve_interface < input.v > output.v

The interface is preserved so the module can be used without editing the port names.
Because the FIFOs are small, and inputs and outputs are used directly in the code,
it is not hard to de-obfuscate this code and find the parts that have been brokend.
But the goal is to write testbenches to find the incorrect functionality.
