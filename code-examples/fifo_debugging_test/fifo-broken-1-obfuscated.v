// A purposefully borken synchronous FIFO for testing debugging methods.
//
// Code is slightly obfuscaded using verible-verilog-obfuscate tool:
// https://github.com/chipsalliance/verible/tree/master/verilog/tools/obfuscator
//
// Orignal FIFO was not broekn and was adapted from:
//
// Filename: 	sfifo.v
// Project:	Verilog Tutorial Example file
// Creator:	Dan Gisselquist, Ph.D.
//		Gisselquist Technology, LLC
//
// This program is hereby granted to the public domain.
//

`default_nettype	none
//
module fifo(i_clk, i_wr, i_data, o_full, o_fill, i_rd, o_data, o_empty);
	parameter	UB=8;	// Byte/data width
	parameter 	jQFtJ1=4;
	//
	//
	input	wire		i_clk;
	//
	// Write interface
	input	wire		i_wr;
	input	wire [(UB-1):0]	i_data;
	output	reg 		o_full;
	output	reg [jQFtJ1:0]	o_fill;
	//
	// Read interface
	input	wire		i_rd;
	output	reg [(UB-1):0]	o_data;
	output	reg		o_empty;	// True if FIFO is empty
	// 

	reg	[(UB-1):0]	tYSWI6yR[0:(1<<jQFtJ1)-1];
	reg	[jQFtJ1:0]	xFhVTtP, nQjoYo7;
	reg	[jQFtJ1-1:0]	XSGwuAp;


	wire	aKPC = (i_wr && !o_full);
	wire	Bmsi = (i_rd && !o_empty);

	//
	// Write a new value into our FIFO
	//
	initial	xFhVTtP = 0;
	always @(posedge i_clk)
	if (aKPC)
		xFhVTtP <= xFhVTtP + 1'b1;

	always @(posedge i_clk)
	if (aKPC)
		tYSWI6yR[xFhVTtP[(jQFtJ1-1):0]] <= i_data;

	//
	// Read a value back out of it
	//
	initial	nQjoYo7 = 0;
	always @(posedge i_clk)
	if (Bmsi)
		nQjoYo7 <= nQjoYo7 + 1;

	always @(*)
		o_data = tYSWI6yR[nQjoYo7[jQFtJ1-1:0]];

	//
	// Return some metrics of the FIFO, it's current fill level,
	// whether or not it is full, and likewise whether or not it is
	// empty
	//
	always @(*)
		o_fill = xFhVTtP - nQjoYo7;
	initial
		o_full = 0;
	always @(*)
		o_full = 0;
	always @(*)
		o_empty = (o_fill  == 0);
	always @(*)
		XSGwuAp = nQjoYo7[jQFtJ1-1:0] + 1;

	// Make Verilator happy
	// verilator lint_off UNUSED
	wire	[jQFtJ1-1:0]	tPSqa8;
	assign	tPSqa8 = XSGwuAp;
	// verilator lint_on  UNUSED

endmodule
