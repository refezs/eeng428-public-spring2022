// A purposefully borken synchronous FIFO for testing debugging methods.
//
// Code is slightly obfuscaded using verible-verilog-obfuscate tool:
// https://github.com/chipsalliance/verible/tree/master/verilog/tools/obfuscator
//
// Orignal FIFO was not broekn and was adapted from:
//
// Filename: 	sfifo.v
// Project:	Verilog Tutorial Example file
// Creator:	Dan Gisselquist, Ph.D.
//		Gisselquist Technology, LLC
//
// This program is hereby granted to the public domain.
//

`default_nettype	none
//
module fifo(i_clk, i_wr, i_data, o_full, o_fill, i_rd, o_data, o_empty);
	parameter	et=8;	// Byte/data width
	parameter 	UXM9v8=4;
	//
	//
	input	wire		i_clk;
	//
	// Write interface
	input	wire		i_wr;
	input	wire [(et-1):0]	i_data;
	output	reg 		o_full;
	output	reg [UXM9v8:0]	o_fill;
	//
	// Read interface
	input	wire		i_rd;
	output	reg [(et-1):0]	o_data;
	output	reg		o_empty;	// True if FIFO is empty
	// 

	reg	[(et-1):0]	ilxfxsjQ[0:(1<<UXM9v8)-1];
	reg	[UXM9v8:0]	nNOCFJR, j41uZhu;
	reg	[UXM9v8-1:0]	K8rjNID;


	wire	Zgm8 = (i_wr && !o_full);
	wire	iQPa = (i_rd && !o_empty);

	//
	// Write a new value into our FIFO
	//
	initial	nNOCFJR = 0;
	always @(posedge i_clk)
	if (Zgm8)
		nNOCFJR <= nNOCFJR + 1'b1;

	always @(posedge i_clk)
	if (Zgm8)
		ilxfxsjQ[nNOCFJR[(UXM9v8-1):0]] <= i_data;

	//
	// Read a value back out of it
	//
	initial	j41uZhu = 0;
	always @(posedge i_clk)
	if (iQPa)
		j41uZhu <= j41uZhu + 1;

	always @(*)
		o_data = ilxfxsjQ[j41uZhu[UXM9v8-1:0]];

	//
	// Return some metrics of the FIFO, it's current fill level,
	// whether or not it is full, and likewise whether or not it is
	// empty
	//
        always @(*)
                o_fill = nNOCFJR - j41uZhu;
        always @(*)
                o_full = o_fill == { 1'b1, {(UXM9v8){1'b0}} };
	initial
		o_empty = 1;
        always @(*)
                o_empty = 1;
	always @(*)
		K8rjNID = j41uZhu[UXM9v8-1:0] + 1;

	// Make Verilator happy
	// verilator lint_off UNUSED
	wire	[UXM9v8-1:0]	mjKide;
	assign	mjKide = K8rjNID;
	// verilator lint_on  UNUSED

endmodule
