// simulation for synchronous FIFO

#include <stdlib.h>
#include <iostream>
#include <verilated.h>
#include <verilated_vcd_c.h>
#include "Vfifo.h"

// max simulation time
#define MAX_SIM_TIME 100

// keep track of simulation time
vluint64_t sim_time = 0;
vluint64_t posedge_cnt = 0;

// main simulation function
int main(int argc, char** argv, char** env) {

    // design under test
    Vfifo *dut = new Vfifo;

    // dump signal traces into a VCD file
    Verilated::traceEverOn(true);
    VerilatedVcdC *m_trace = new VerilatedVcdC;
    dut->trace(m_trace, 5);
    m_trace->open("fifo-tb-waveform.vcd");

    // simulate the design
    while (sim_time < MAX_SIM_TIME) {
        dut->i_clk ^= 1;
        dut->eval();
        if(dut->i_clk == 1){
            posedge_cnt++;
        
            // write a piece of data
            if(posedge_cnt==2){
                dut->i_wr = 1;
                dut->i_data = 10;
            }
        
            if(posedge_cnt==3){
                dut->i_wr = 0;
                dut->i_data = 0;
            }
        
            // more testing can be added here...
        
        }
        m_trace->dump(sim_time);
        sim_time++;
    }

    // finish simulation
    m_trace->close();
    delete dut;
    exit(EXIT_SUCCESS);
}

 
