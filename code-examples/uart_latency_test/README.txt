# Builing hardware design

Follow the building instuctions as for the loopback example, e.g.:

./make_build_one.py kc705

# Programming the FPGA board

Follow the programming instructions as for the loopback example, e.g.:

./make_program.py -d ../build/kc705/ -b kc705-01 -j ~/ports.json

# Communicating with the hardware using the uart_latency_test.py

First, the test script sends an random byte with value between 0 and 127 and expects the FPGA to respond with
the value incremented by 1.  I.e. if you send byte with value 65, then the script
expects the FPGA to send back byte with value 66 = 65 + 1.

Second, the test expects another byte, which is the latency count sent by the finite state machine on the FPGA.
If the second byte is not received, the Python script will get stuck, i.e. blocked, waiting for the serial
data to arrive.

To run the script, you need to set the baud rate and the serial port.  Baud rate can be found
from the top-level Verilog file.  The serial port can be found from the ports.json file.
Example command:

./uart_latency_test.py -b 921600 -p /dev/ttyUSB0

The test script should finish with 'Test ended.' message. The script does not check what value
the hardware returned for latency.
