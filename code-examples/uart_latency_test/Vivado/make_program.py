#!/usr/bin/python3

import os
import sys
import json
import argparse
import re
import subprocess

# Default Vivado tools path
vivado = '/tools/Xilinx/Vivado/2021.1/bin/vivado'

# Try to detect if hw_server is running, if so change Vivado tools path to math tools versions
returned_text = subprocess.check_output("ps -aux | grep hw_server", shell=True, universal_newlines=True)
for line in returned_text.splitlines():
    m = re.match(".*(20[0-9][0-9].[0-9]).*hw_server.*TCP", line)
    if( m != None ):
        version = m.group(1)
        vivado = '/tools/Xilinx/Vivado/' + str(version) + '/bin/vivado'
        break

# Get command line arguments
parser = argparse.ArgumentParser(description='Program the FPGA given build directory with a *.bit file, FPGA board type, and location of the JSON file with FPGA board information.')
parser.add_argument('-d', '--dir', default=None, type=str, required=True, help='Location of the build directory.')
parser.add_argument('-b', '--board', default=None, type=str, required=True, help='FPGA board type.')
parser.add_argument('-j', '--json', default=None, type=str, required=True, help='Location of the ports.json file, usually in /home/$USER/ccf/ports.json')
args = parser.parse_args()

build_dir = args.dir
board = args.board
json_file = args.json

# Get the device name needed by the programming tools
with open(os.path.join(str(build_dir), 'device.txt'), 'r') as f:
    device = f.readline().rstrip('\n')

# Get serial number of the programmer
ports_json_file = open(json_file, 'r')
ports_json_data = json.load(ports_json_file)
ports_json_file.close()
programmer_serial_number = ports_json_data[board]['programmer_serial_number']

# Set the bitstream file path
bitstream_file = os.path.join(build_dir, 'synth_system.bit')

# Program the device
print('Programming ' + device + ' using bitstream ' + bitstream_file + '...')
command = '' + vivado + ' -nojournal -nolog -mode batch -source program.tcl -tclargs ' + device + ' ' + programmer_serial_number + ' ' + bitstream_file + ' | tee -a ' + build_dir + '/program-report.txt'
os.system(command)

#with open(os.path.join(build_dir, 'program.log'), 'w') as outfile:
#    subprocess.call([vivado, '-nojournal', '-nolog', '-mode', 'batch', '-source', 'program.tcl', '-tclargs', '{}'.format(device), '{}'.format(bitstream_file)], stdout=outfile, stderr=outfile, cwd=os.getcwd())

# Done.
print("Done.")
