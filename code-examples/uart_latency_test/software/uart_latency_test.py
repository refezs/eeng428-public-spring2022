#!/usr/bin/python3.6

import time
import serial
import os
import sys
import argparse

# Get command line arguments
parser = argparse.ArgumentParser(description='Serial port latency tester script.')
parser.add_argument('-b', '--baud', default=None, type=str, required=True, help='Baud rate of the serial port.')
parser.add_argument('-p', '--port', default=None, type=str, required=True, help='Serial port device name.')
args = parser.parse_args()

baud = args.baud
port = args.port

# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(
    port=port,
    baudrate=baud,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.EIGHTBITS
)

# Allow time for serial to intialize
time.sleep(0.2)

# Specify how many times to send random bytes to the FPGA,
# also keep track of number of correct replies
counter_max = 5
counter = 0
num_match = 0

# Send random byte to FPGA to trigger the FPGA state machine
while True:
    byte = os.urandom(1)
    if( byte <= (127).to_bytes(1, byteorder='big') ):
        break

print( 'Sent:                       ' + str(ord(byte)) )
ser.write(byte)

print( 'Expecting incremented byte: ' + str(int.from_bytes(byte, 'big') + 1) )
c = ser.read()
print( 'Got:                        ' + str(ord(c)) + '\n' )

print( 'Expecting latency count...' )
c = ser.read()
print( 'Got:                        ' + str(ord(c)) + '\n' )

print( 'Test ended.' )
sys.exit(0)
