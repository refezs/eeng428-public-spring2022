#!/bin/bash

boards="kc705 kcu1500 vcu118 vcu1525"

echo 'Started parallel build...'
echo''

date

parallel --verbose --jobs 3 './make_build_one.py  > /dev/null' ::: $boards

date

echo''
echo 'Check build directories for output.'
