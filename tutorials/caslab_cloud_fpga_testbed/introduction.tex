
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}
\label{sec:introduction}

CASLAB Cloud FPGA (CCF) testbed is a set of remotely-accessible FPGAs.  The FPGAs can be power on and off through
a remote-controlled power supply, and USB connections between the server
and FPGAs (for JTAG port and Serial port)  are interfaced through a remote-controlled
USB hub.  The FPGAs are assigned to users on first-come, first-serve basis,
and are controlled by the {\tt control\_fpga.py} script.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Accessing CASLAB Cloud FPGA Testbed}

Users should use SSH to connect to \url{caslab-cloudfpga.eng.yale.internal} server. Below command can be used from the terminal
to make the SSH connection.

\begin{verbatim}
    $ ssh -X <username>@caslab-cloudfpga.eng.yale.internal
\end{verbatim}

To access the server, users need to be on Yale's network or use Yale's VPN, otherwise the server
is not accessible due to private IP address and hostname.

Users can log in using username and password, or using a SSH key for password-less login
(users should first log in using their username and password and then copy their SSH key into
{\tt /home/\$USER/.ssh/authorized\_keys} file to enable password-less login, note that the {\tt\$USER} variable
is automatically replaced with the user's actual username on most Linux systems).

Once logged in, users can run {\tt control\_fpga.py} command from any location to start, stop, or get FPGA status.

\subsection{Connecting to the Testbed using Visual Studio Code's Terminal}

The testbed can be accessed using SSH from any terminal program, including terminal built into Visual Studio Code.
Please read the Appendix for information about setting up Visual Studio Code. All the commands listed
in the remainder of this tutorial can be typed in the terminal in Visual Studio Code, or any other terminal application.

For the X Window forwarding to work with the {\tt -X} SSH option, the user's local computer has to have X Window manager installed.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Controlling FPGAs in the Testbed}

Below subsections describe now to enable, disable, or get information about the FPGAs.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Using {\tt control\_fpga.py} Script}

The {\tt control\_fpga.py} script is used to control FPGAs, it can be always used with {\tt --help} flag to see all the options
available to users.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Listing FPGAs and Getting FPGA Status}

To get the list of all installed FPGAs, the {\tt --list} option can be used:

\begin{verbatim}
    $ control_fpga.py --list
\end{verbatim}

To check which FPGAs are available and which are in use, the {\tt --getstatus} option can be used:

\begin{verbatim}
    $ control_fpga.py --getstatus
\end{verbatim}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Enabling an FPGA: Board Selection and Notification E-mails}

To enable an FPGA, select the desired FPGA with the {\tt --fpga} option and use the {\tt --enable} flag.
If the board is in use, the script will stop, and generate an exit code $1$ indicating an error.

A typical command to enable a board is below, here the board name
{\tt CASLAB-FPGA-KC705-02} can be replaced with the user's desired board:

\begin{verbatim}
    $ control_fpga.py --fpga CASLAB-FPGA-KC705-02 --enable
\end{verbatim}

In addition, when a board is enabled, by default, an e-mail is sent to all the users who subscribed to get notifications about the select board.
The notification can be overwritten with the {\tt --nonotify} option, which prevents e-mail from being sent.
For example, if other users are aware that this board is in use, notification e-mails may be disabled,
or if the board is turned on and off very frequently, the notification may be disabled to prevent other users from getting spammed
with notification e-mails.

If a user wants to subscribe or unsubscribe to receiver notifications about a specific board, they
can use the {\tt --subscribe} or {\tt --unsubscribe} options respectively.

For example, to subscribe to a board, use the command:

\begin{verbatim}
    $ control_fpga.py --fpga CASLAB-FPGA-KC705-02 --subscribe
\end{verbatim}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{JSON File with Information about User's Active FPGAs}

When an FPGA is enabled, there will be a {\tt ports.json} file created in the home directory of the user, i.e.
the file will be located in {\tt /home/\$USER/ports.json}. This is a JSON type file that can be viewed with any text editor.
The file contains the serial number of the FPGA board
(needed later for selecting the correct board for programming with Xilinx tools),
information on whether notification e-mails should be sent (this file is referenced and used to control if e-mail should be sent
when FPGA is being disable), and
most importantly the USB port device name for the USB port used for Serial communication with the FPGA.

Sample contents of a {\tt ports.json} file are:

\begin{verbatim}
{
    "kc705-02": {
        "programmer_serial_number": "Digilent/210203A7D6C2A",
        "uartport": "/dev/ttyUSB0",
        "send_notification_email": "yes"
    }
}
\end{verbatim}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Programming and Using FPGAs}

Once an FPGA is enabled, the user has full access and control over the FPGAs.  The first step is usually to program the FPGA.
The {\tt programmer\_serial\_number} from the JSON file
should be used in Xilinx tools to specify the FPGA board that is to be programmed; Xilinx tools use the serial number, not the FPGA name,
to control which FPGA is programmed.  The serial number is also used for identification when using JTAG to communicate with the FPGA board.

Once programmed, the FPGA usually will have serial port module, also called UART, so that software on the server, e.g., a Python script,
can send and receive commands or data over the serial port.

If user's FPGA design includes a serial port, user of course needs to have serial port controller module loaded into the FPGA, and then use the {\tt uartport}
specified in the JSON on the server side to connect to the correct serial port.

An example of a very simple FPGA design with a serial port is discussed later in Section~\ref{sec_serial_loopback}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Disabling an FPGA}

When a user is done with the board, it should be disabled so other users can use it.
The board can be disabled by selecting the desired FPGA with the {\tt --fpga} option and useing the {\tt --disable} flag:

\begin{verbatim}
    $ control_fpga.py --fpga CASLAB-FPGA-KC705-02 --disable
\end{verbatim}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Serial Port Example with the Serial Port Loopback Test}
\label{sec_serial_loopback}

As an example of using the CASLAB Cloud FPGA testbed, and also for debugging the FPGAs, users
can use a simple serial port loopback design to test FPGA connectivity.  The loopback test consists of a simple
hardware design that receives serial port data bytes from the host computer (on the RX, receive, port), increments each byte by $1$,
and sends them back (on the TX, transmit, port).
The loopback test also includes a simple Python script that is used to send data bytes to the FPGA over serial port,
and to receive bytes from the FPGA, and check that the received bytes are same as the sent byte but incremented by $1$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Loopback Test Hardware Design Files}

To access the loopback test hardware and software, the users should request access to a code repository
that contains the example.
Inside the repository are all the testbed controller script files as well as the loopback example, some of
the relevant files and directories in the repository~are:

\begin{verbatim}
.
|-- boards
|-- boards.json
|-- README.txt
|-- software
|   \-- uart_loopback_test.py
|-- build
|-- verilog
|   |-- rxuart.v
|   |-- txuart.v
|   \-- uart_loopback_top.v
\-- Vivado
\end{verbatim}

The {\tt boards} directory contains XDC (Xilinx Design Constraints) files for each FPGA board type supported by the loopback test,
there should be one for each FPGA in the testbed, users may request the files for specific boards if they are not in the directory.
The {\tt boards.json} file contains information about each board type: FPGA chip's part name, device name,
location of the XDC file for this board type, and the clock frequency of the main reference clock on the FPGA.
The {\tt software} directory contains the software used with communicating with the hardware.
The {\tt build} directory will only appear once the design is synthesized, it contains bitstream files.
The {\tt verilog} directory contains the Verilog source files for the loopback hardware test.
The {\tt Vivado} directory contains the scripts and other configuration files needed to synthesize the hardware design, and program it on the FPGA.

Again, note that the {\tt build} directory is not part of the repository, it will be created during synthesis of the hardware design, any synthesized
files should not be put in the repository as they can always be re-generated from the source code.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Synthesizing Loopback Test Hardware}

To synthesize a design for a specific FPGA board type (one of the boards listed in the {\tt boards} directory and the {\tt boards.json} file),
the {\tt make\_build\_one.py} script can be used from the {\tt Vivado} directory.  It requires an argument, which is the board type, the board type is always
in lower case.  For example to synthesize a design to be used with the CASLAB-FPGA-KC705-02 board,
the board type used should be {\tt kc705}:

\begin{verbatim}
    $ ./make_build_one.py kc705
\end{verbatim}

If there is an error about missing license file or not supported features, please check that the {\tt XILINXD\_LICENSE\_FILE}
environmental variable is defined.  On many Linux systems you can use the {\tt env} command to list the environmental variables.
Please reach out with questions if license file or environmental variable is missing.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{The Build Directory}

Once synthesized, the resulting design will be placed in the {\tt build} directory inside a folder named
after the board type, e.g., a {\tt build/kc705} directory will be created with files for the KC705 board.
The bitstream file
that should be loaded onto the FPGA is the {\tt synth\_system.bit} file in that folder.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Programming the FPGA with the Loopback Test Design}

In the {\tt Vivado} directory use the {\tt make\_program.py} to program the FPGA.
It has three required arguments: location of the build directory, board instance name, and path to the JSON file
with the FPGA board information.  Sample programming command for CASLAB-FPGA-KC705-02 is below.
Note that the board type name in this case is {\tt kc705} while the specific instance is {\tt kc705-02} (in future
there may be more instances of this board which would be labeled {\tt kc705-03}, {\tt kc705-04}, etc.

\begin{verbatim}
    $ ./make_program.py -d ../build/kc705 -b kc705-02 -j /home/$USER/ports.json
\end{verbatim}

If there is an error about {\tt hw\_server} version, the script may be edited to change the Vivado version,
the Vivado version used to program cannot be newer than the version of the currently running  {\tt hw\_server}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Running Software Test for the Loopback Design}

The {\tt uart\_loopback\_test.py} script in the {\tt software} directory of the repository can be used to test the FPGA
and check if the serial port and loopback are working.  The script has two arguments, the serial port to use,
and the baud rate.  The serial port can be obtained from {\tt ports.json} file.  The baud rate is hard-coded
in the loopback hardware design, currently at 921600 baud.  If the script seems not responding and does not
display ``Test passed.'' likely issue is wrong baud rate or problem with the hardware design.

First check the serial port to be used by viewing the contents of the {\tt ports.json} file:

\begin{verbatim}
    $ cat /home/$USER/ports.json
\end{verbatim}

Assuming the above command displays {\tt "uartport": "/dev/ttyUSB0"}, then an example of using the software
test script is below:

\begin{verbatim}
    $ ./uart_loopback_test.py -b 921600 -p /dev/ttyUSB0
\end{verbatim}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Optional: Creating Loopback Test for a New FPGA Board}

To adapt the loopback test to a new board, for example when a new FPGA board is added to the testbed, a number
of files in loopback test example need to be modified.

Although the Verilog code should not require modification, for each new board and XDC file is required that
specifies the clock pins, UART pins (serial port), and the clock frequency of the default crystal oscillator on the board.
The XDC file should be placed in the {\tt boards/<boardname>} directory, for example for adding VCU1525, a new folder
called {\tt boards/vcu1525} should be created, and inside an XDC file should be place.  Name of the file can be freely
selected, for example {\tt vcu1525\_loopback\_test.xdc}.  It should have contents similar to below example:

\begin{verbatim}
# Differential pair clock pins
set_property -dict {PACKAGE_PIN AY37 IOSTANDARD DIFF_SSTL15} [get_ports clk_p]
set_property -dict {PACKAGE_PIN AY38 IOSTANDARD DIFF_SSTL15} [get_ports clk_n]

# Clock pin's clock period, here 3.333 ns = 300 MHz, need to set for one of the clock pins
create_clock -period 3.333 -name clk -waveform {0.000 1.667} -add [get_ports clk_p] 

# UART receive and transmit pins
set_property -dict {PACKAGE_PIN BF18 IOSTANDARD LVCMOS12} [get_ports uart_rx]
set_property -dict {PACKAGE_PIN BB20 IOSTANDARD LVCMOS12} [get_ports uart_tx]
\end{verbatim}

The specific pin names and I/O standards can be often found in the reference XDC files
provided for each FPGA board.  The port names, e.g., {\tt clk\_p}, should not be changed as
they need to match ports in the Verilog code.

After defining the XDC file, the {\tt boards.json} file needs to be updated to add information about the board.  Example entry
for a board is below:

\begin{verbatim}
    "vcu1525": {
        "partname": "xcvu9p-fsgd2104-2L-e",
        "device": "xcvu9p",
        "xdcfile": "../boards/vcu1525/vcu1525_loopback_test.xdc",
        "input_clk_freq_mhz": "300"
    }
\end{verbatim}

The {\tt partname} is the specific FPGA chip on the board.  The {\tt device} is the FPGA chips's device family name,
usually it is the prefix of the {\tt partname}.  The {\tt xdcfile} should point to the XDC file that was added in the {\tt boards} directory.
Finally, the {\tt input\_clk\_freq\_mhz} should be the clock pin's frequency.  It is used by the hardware code to correctly set serial port's frequency.

With the above additions, the loopback test should be able to be synthesized using the scripts in the {\tt Vivado} folder.
The software testing script {\tt uart\_loopback\_test.py} should not require any updates.
