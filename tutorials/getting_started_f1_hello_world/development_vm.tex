

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Creating Development VM Instance}
\label{sec_development_vm}

This section covers steps needed to create a {\tt c4.4xlarge} VM instance
that will be used to synthesize the hardware design (and optionally in future compile C code for
the software).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Create {\tt c4.4xlarge} Instance}

The development, compilation, and synthesis of the FPGA code (and C code, if necessary) is done using a VM
running on a {\tt c4.4xlarge} instance in Amazon's cloud computing data centers.
To create a development VM for working with C and FPGA code, sign into the AWS console: \url{https://console.aws.amazon.com}.
In the top-right corner make sure you're in {\tt US East (N. Virginia)} region, Amazon EC2 F1 compute instances 
are not available in all regions\footnote{Also, selecting instance in region which is physically close to your location
should reduce network delays and improve connection speed.}.

From AWS console go to the {\tt EC2 Dashboard} and then go to the {\tt Instances} page.  Now you can select {\tt Launch Instances} and begin the process
of configuring and launching an Amazon EC2 compute instance.  Do not skip an configuration steps and do not press {\tt Launch Instance} under the 
{\tt Summary} section before Step 7.

\begin{itemize}
\item \textbf{Step 1: Name And Tags} -- If on a University-based account, create a name for your instance that will allow you to distinguish your instance 
from other students' instances. There is no need to add additional tags.
\item \textbf{Step 2: Application and OS Images} -- Search for ``FPGA'' under AWS Marketplace AMIs and select the ``FPGA Developer AMI.''
\item \textbf{Step 3: Instance Type} -- The recommended instance type is {\tt c4.4xlarge} for synthesizing your FPGA logic, also called Custom Logic (CL) by Amazon.
Other instance types can be used, but CPU, memory, and disk type will affect synthesis speed.
\item \textbf{Step 4: Key Pair} -- If you do not have a key pair yet, create a new key pair with a custom name, RSA encryption, and {\tt .pem} file format. 
Select the {\tt .ppk} file format instead if using PuTTY on Windows. Please read the additional instructions on creating a key pair in the immediate next section 
before continuing to Step 5.
Windows PuTTY to connect remotely to the development VM environment.
\item \textbf{Step 5: Network Settings} -- The default configuration should be sufficient. No action required.
\item \textbf{Step 6: Configure Storage} -- Change the size of the root volume to 128 GiB to ensure sufficient space to download and synthesize code.
\item \textbf{Step 7: Advanced Details} -- The default configuration should be sufficient. No action required.
\end{itemize}

Ensure under the {\tt Summary} section that the number of instances is 1 and that the instance options correspond to the ones selected in the steps above. Once you 
have confirmed that the instance has been configured properly, select {\tt Launch Instance} under the {\tt Summary} section to start the instance.

You can monitor the status of the instance on the {\tt EC2 Dashboard} on the {\tt Instances} page.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Creating a Key Pair}

Instead of passwords, the virtual machines (VMs) in Amazon's cloud deployments
use cryptographic keys for authenticating users and letting you log into the virtual machine
via SSH connection.  A cryptographic key pair consists of a public key that AWS stores, and a private key that you store (in a file). 
Together, they allow you to connect to your instance securely.

If you don't have an existing key file, or if the file is lost, you will have had to select {\tt Create a new key pair}
in Step 4 when configuring a new VM instance to launch.
Make sure you have entered a name for the key and selected {\tt Download Key Pair}
to download the key to your computer. 

After downloading the file, ensure to change permissions so only you, 
and not other users on your computer, can access the file.  Anybody with access to the file, or copy of the file, can log into your virtual machines.  The file
needs to be protected.  On Linux, you can use command the following command from the local directory that contains the key file: 

\begin{verbatim}
    $ chmod 400 *.pem
\end{verbatim}

A user without specific restricted access permissions to their key file will not be able to connect to the VM instance. 

Windows users may encounter difficulties trying to edit the permissions to the equivalent permissions of {\tt chmod 400} on Linux. 
If you are using Windows with a Subsystem for Linux (WSL) like Ubuntu, the Linux command above may not execute in a Windows directory 
(e.g. {\tt C:\textbackslash Users\textbackslash \%USERNAME\%\textbackslash Downloads\textbackslash}). 
To ensure that the key file permissions are set correctly, move the key file to a WSL directory hidden from Windows. 
For example, you can try running the following command using a WSL terminal from the Windows directory that contains the downloaded key file:

\begin{verbatim}
    $ mv *.pem ~/.ssh/
\end{verbatim}

You can also attempt to replicate the {\tt chmod 400} commmand with either Windows Command Prompt or Windows 
File Explorer according to the suggestions online: \url{https://gist.github.com/jaskiratr/cfacb332bfdff2f63f535db7efb6df93}. 
However, this solution may not be as straight-forward as simply moving the key file to a WSL directory.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Connecting to the Instance}

Once the VM has started and its status has changed to {\tt running} on the {\tt EC2 Dashboard} on the {\tt Instances} page,
you can select the instance by clicking it's name under the {\tt Instance ID}.
On the {\tt Instance summary} page that opens up, check that the instance has
a Public IPv4 address.

If it does not, you will need to create and assign an Elastic IP address to the instance.
Go into the {\tt EC2 Dashboard}. Then in the {\tt Network \& Security} menu, select {\tt Elastic IPs}.
Click on {\tt Allocate Elastic IP address}, in the address settings confirm you are in the right region,
and then click {\tt Allocate} on the bottom of the page.  Now in the {\tt Elastic IP addresses} table,
you should see a new address, select it, then click {\tt Actions} and then {\tt Associate Elastic IP address}.
You can now click the {\tt Instance} box to get a list of instances and select your instance.

On the {\tt Instance summary} page,  the instance should now have a
a Public IPv4 address.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{SSH'ing into the VM}

Now the VM status should still be {\tt running} on the {\tt EC2 Dashboard} under the {\tt Instances} page.
To SSH, you can select the instance, then press {\tt Connect}. 
Each time you start the VM, it will get a different IP address, so you need to check
the {\tt Connect} window to see the proper IP and command-line argument to connect to the instance.  The {\tt SSH client} tab
gives information on how to connect to the instance using SSH and the {\tt *.pem} file that contains your secret key.  

For Linux and WSL users, you are given a command you can cut-and-paste straight into terminal. 
Make sure to run the command from the directory that contains the {\tt *.pem} key file. 

Note that the user name you are using to connect to the VM is by default {\tt root}.
However, for the ``FPGA Developer AMI'' the proper user name is {\tt centos} (the VM uses CentOS Linux operating system distribution).
There should be no password associated with the {\tt centos} user, and the user has root privilege.
E.g. you can execute {\tt sudo} commands without password, such as
{\tt sudo yum install vim} to install software such as the Vim text editor.

Windows PuTTY users may need to follow modified steps to successfully SSH into the VM instance.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{FPGA Development Tools Setup}

Having completed the development VM setup, you can proceed
to Section~\ref{sec_tools_setup} to setup the Hardware Development Kit (HDK) and other tools.
