

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Creating F1 VM Instance with Access to FPGA}

Once an AGFI ID is available, the design can be loaded and tested on an FPGA.
For this purpose you need to start a new VM, and Amazon EC2 F1 instance, that has access
to Amazon's FPGAs.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Requesting Access to F1 Instances}

By default, Amazon EC2 users may not have access to F1 instances.
To get access to F1 instances, you need to request a ``limit increase'' for F1 instances.
Note that permissions may already be granted if using a University-based account.

If using an individual account or issues with F1 instance permissions arise,
log into the AWS EC2 Dashboard, at \url{https://console.aws.amazon.com/ec2}.
On the left-hand side, select {\tt Limits}. You may have to expand the menu bar on the left-hand side
to see the {\tt Limits} link.
Next, select {\tt Running F1 Dedicated Hosts} in the table and click {\tt Request limit increase}.

On the request form, fill in:

\begin{itemize}
\item Limit Type $=$ EC2 FPGA
\item Severity $=$ Important question
\item Region $=$ US East (Northern Virginia)
\item Limit $=$ Number of FPGA Images
\item New limit value $=$ 4 (or 8 or more if you want to run more FPGAs 
in parallel in future\footnote{You may want to keep the number small as a safety feature 
so you do not accidentally start too many FPGA instances in parallel and incur a large cost.})
\item Case description $=$ Mention this request is for class or research project.
\item Preferred contact language $=$ English (or pick your language)
\item Contact method $=$ Web or Phone as you wish
\end{itemize}

Finally, click {\tt Submit} to submit the request and wait for the request to be processed.
It may take a few hours or longer.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Launch an F1 Instance}

An Amazon EC2 F1 instance is needed to run the AGFI on an FPGA.

Follow the same procedure as launching the {\tt c4.4xlarge} instance with the FPGA Developer AMI, 
except this time with a {\tt f1.2xlarge} instance. If you used a local machine for CL development,
then you will need to refer to the 7 steps outlined to start a VM instance.

Curiously, {\tt f1.2xlarge} gives access to 1 FPGA, while {\tt f1.16xlarge} gives access to 8 FPGAs.  
While the naming is confusing, use {\tt f1.2xlarge} as only 1 FPGA board is needed.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{SSH'ing into the VM}

Follow previous instructions to SSH into the F1 VM.

Note, you may thus now have two VMs running in parallel and have two SSH connections, one for each VM.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Setup the FPGA Management Tools and Configure AWS CLI}

Similar to setup of the development VM, you need to download the {\tt aws-fpga} code form Amazon's git repository.
Further, you need to setup the SDK and configure the CLI.

Download {\tt aws-fpga} code again.

\begin{verbatim}
    $ git clone https://github.com/aws/aws-fpga
\end{verbatim}

Then, source the SDK to setup environment.
Sourcing sdk\_setup.sh installs and sets up all the FPGA Management tools necessary for loading and testing your AFI.

\begin{verbatim}
    $ cd aws-fpga/
    $ source sdk_setup.sh
\end{verbatim}

Only SDK is needed on F1 since you're not synthesizing the code, only loading already generated code onto the FPGA.

Furthermore, you need to configure AWS CLI again, since this time you're working in a new VM, the F1 instance.
As before use {\tt aws configure} to provide necessary information.  Again, use {\tt us-east-1} region
and {\tt json} as output file type.

\begin{verbatim}
    $ aws configure
\end{verbatim}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Load the AGFI onto FPGA}

You can now use the FPGA Management tools, from within your F1 instance, to load your AFI onto an FPGA on a specific slot. 
Since we are using an F1 instance with 1 FPGA, there is only 1 slot, whereas other instance types will have more slots.

Make sure you clear any AFI you have previously loaded in your slot:
\begin{verbatim}
    $ sudo fpga-clear-local-image -S 0
\end{verbatim}

Now, load your AFI to FPGA slot 0.  Make sure to change the AGFI ID to the actual AGFI ID for
your generated design.

\begin{verbatim}
    $ sudo fpga-load-local-image -S 0 -I agfi-0fcf87119b8e97bf3
\end{verbatim}

You can verify that the AFI was loaded properly if the output shows the FPGA in the {\tt loaded} state after the FPGA image load operation.

If you receive receive the following error, and you are sure that you have used the correct AGFI, 
`you may need to grant yourself permission to load the AFI:

\begin{verbatim}
    Error: (5) invalid-afi-id
    The agfi id passed is invalid or you do not have permission to load the AFI.
\end{verbatim}

You can grant yourself permission with the following command, substituting {\tt $<$AFI$>$} with your Hello World AFI and {\tt $<$Account ID$>$} with the
account ID that can be found as a numerical string at the very top right-hand corner of the Amazon EC2 Console.

\begin{verbatim}
    $ aws ec2 --region us-east-1 modify-fpga-image-attribute \
      --fpga-image-id <AFI> \
      --operation-type add --user-ids <Account ID>
\end{verbatim}

\subsubsection{Pre-synthesized Hello World Example}

Amazon maintains pre-synthesized hello world example.
A pre-generated Hello World example can be accessed using public AGFI number.
The pre-generated AFI ID for hello world example is {\tt afi-03d11a4ea66e883ef}.
The pre-generated AGFI ID for hello world example is {\tt agfi-0fcf87119b8e97bf3}.

You can use these to compare the behavior of your generated example with the pre-generated examples.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Validating the Design Using the CL Example Software}

Each CL example comes with a runtime software under 
{\tt \$CL\_DIR/software/runtime/} subdirectory. 
The {\tt \$CL\_DIR} is the directory of the custom logic you are using. For the
hello world example, it will typically be in the examples directory.

\begin{verbatim}
    $ cd /home/$USER/aws-fpga/hdk/cl/examples/cl_hello_world
    $ export CL_DIR=$PWD
\end{verbatim}

Once you locate the hello world example, 
you will need to build the runtime application that matches your loaded Hello World AFI.

\begin{verbatim}
    $ cd $CL_DIR/software/runtime/
    $ make all
    $ sudo ./test_hello_world
\end{verbatim}

To execute {\tt ./test\_hello\_world} application you need sudo privileges as the code
requires access to the PCI express bus, which is usually restricted.  Within the example software, you can use different commands
to communicate with the example hardware to get some outputs from the Hello World CL loaded on the FPGA.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Allow Other Users to Access Your AGFI}

Once an AGFI is created, it can be used by other users, or by the instructor to test your code.
To allow others to access your AGFI, you need to change the permissions.
There is a modify load permissions command which can be run as follows to
allow a specific user access to your AGFI.

To add user ID (AWS account ID) with permissions to load a specific AGFI, use following command.
Make sure to change the AFI ID, and the user ID as needed.

\begin{verbatim}
    $ aws ec2 --region us-east-1 modify-fpga-image-attribute \
      --fpga-image-id afi-0e5361a69d2af203d \
      --operation-type add --user-ids 095707098027
\end{verbatim}

The output of the command should be similar to:

\begin{verbatim}
      { "FpgaImageAttribute": { "FpgaImageId": "afi-0e5361a69d2af203d",
      "LoadPermissions": [ { "UserId": "095707098027" } ] } }
\end{verbatim}

Note that above will add the permissions to the AFI not the AGFI.
But the permissions for the AFI will allow access to the AGFI as well.
There is also a command to make the AFI and AGFI public which can be found
at \url{https://github.com/aws/aws-fpga/blob/master/hdk/docs/fpga_image_attributes.md}.

{\bf Upon completion of this tutorial, add the load permissions for the instructor so your compiled
{\em hello world} example can be tested.  Instructor's AWS account ID will be provided to the class by e-mail.}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Shutting Down VM when Done Working}

When you are done working, or are taking a break, make sure to shut down the VMs, as the VM usage
is charged by hour, independent of whether you are actually using the VM or the VM has just been left running idle.
Do not stop the VM while synthesizing the FPGA code, as that will stop the build process. However,
once the tarball file is uploaded into S3 bucket, the VM can be shutdown while waiting for Amazon to approve the design.

Before shutting down the VM, make sure to save all the files, upload any modified files to git, and then log out.
Also, clear the FPGA slot before shutting things down if this is an F1 instance.

To shut down the VM, go to the {\tt EC2 Dashboard} and the {\tt Instances} page.
Select the instance, then press {\tt Actions}
and then press {\tt Instance State} and then press {\tt Stop}.
Once the VM has stopped its status will change to {\tt stopped}.
Shutting down the VM is similar to turning off the computer, next time you start up the VM, all the files and configuration
should be there (except data or files stored to the ephemeral drive, which is erased each time).

