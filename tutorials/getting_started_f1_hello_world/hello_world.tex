

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Hello World Example}
\label{sec_hello_world}

Having setup the development environment and development tools,
this section discusses how to compile and synthesize a {\em Hello World}
example.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Select Hello World Example for Synthesis}

Use the commands below to select the Hello World example to be your Custom Logic directory and set your email address
to receive notifications.  Note, sometimes the notification by e-mail does not work, usually if there is some
error in compilation or environment is not setup correctly.  Always check manually if the compilation is done.

\begin{verbatim}
    $ export HDK_DIR=$(pwd)/hdk
    $ cd $HDK_DIR/cl/examples/cl_hello_world    
    $ export CL_DIR=$(pwd)
    $ export EMAIL=your.email@example.com
\end{verbatim}

Setting up the {\tt CL\_DIR} environment variable is crucial as the build scripts rely on that value to locate
the code that should be synthesized.
Each {\tt CL\_DIR} reference follows the recommended directory structure to match the expected structure for the HDK
simulation and build scripts. 
The {\tt EMAIL} environment variable is used to notify you when your Custom Logic build has completed.

Note, first time you synthesize a design and request an e-mail, you may get an e-mail
from Amazon about {\em AWS Notification - Subscription Confirmation}.  You need to
confirm your subscription to the notification e-mails before you will actually be able to receive them.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Start the Build Process for the Custom Logic}

Executing the {\tt aws\_build\_dcp\_from\_cl.sh}
script will perform the entire synthesis process converting the CL design into a completed Design Checkpoint
that meets timing and placement constrains of the target FPGA. The output is a tarball file (.tar) comprising the DCP file, and other log/manifest files, 
formatted as {\tt YY\_MM\_DD-hhmm.Developer\_CL.tar}.
This file would be submitted to AWS to create an AFI. 
By default the build script will use Clock Group A Recipe A0 which uses a main clock of 125 MHz.
I.e. your custom logic will be running at 125 MHz.

Use the commands below to start the build process:

\begin{verbatim}
    $ cd $CL_DIR/build/scripts
    $ ./aws_build_dcp_from_cl.sh -notify
\end{verbatim}

Once the build process is started, you may disconnect from the VM (e.g. end the SSH session), and re-connect later
to check on the build process or to continue with the generation of the AFI image if the build finished successfully.  Note, however,
the VM cannot be stopped so that the compilation process continues.
Also note, the design may take a few hours to synthesize -- once done, the VM can be stopped if you don't want to immediately
proceed with the AFI creating steps.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Monitoring Build Process}

Since the DCP generation can take up to several hours to complete, the {\tt aws\_build\_dcp\_from\_cl.sh}
will run the main build process (Vivado) within a {\tt nohup} context:
This will allow the build to continue running even if the SSH session is terminated half way through the run, for example.
The {\tt -notify} flag indicates to the script to notify you at your EMAIL address when the build is completed.

If you do not receive the notification e-mail, check if the {\tt *.tar} file is generated in the \\
{\tt \$CL\_DIR/build/checkpoints/to\_aws} directory.

If the compilation fails, no {\tt *.tar} file will be generated.
You can find logs of the most recent compilation in the {\tt \$CL\_DIR/build/scripts} folder.
You may need to read the log files to find the source of the error.

When build process is done successfully, the final {\tt *.tar} file should be available in sub directory of CL\_DIR:

\begin{verbatim}
    $ cd $CL_DIR/build/checkpoints/to_aws
\end{verbatim}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Submit the DCP Tarball to AWS to Create the AFI}

Once you have the {\tt *.Developer\_CL.tar}, which contains the DCP (Design Checkpoint), you can being AFI creation.
To submit the DCP, use the S3 bucket and upload the tarball file into that bucket. 
You need to prepare the following information:

\begin{enumerate}
\item Name of the logic design (Optional).
\item Generic description of the logic design (Optional).
\item Location of the tarball file within S3 bucket.
\item Location of an S3 directory where AWS would write back logs of the AFI creation.
\item AWS region where the AFI will be created. 
An AFI needs to be created in same region as the F1 instance that will use it.
Use {\tt copy-fpga-image} AWS CLI command to copy an AFI to a different region if needed.
\end{enumerate}

To upload your tarball file to S3,
use the bucket and folder you created initially for your tarball, then copy files into S3
using a single command:

\begin{verbatim}
    $ aws s3 cp $CL_DIR/build/checkpoints/to_aws/*.Developer_CL.tar \
      s3://<bucket-name>/<dcp-folder-name>/
\end{verbatim}

Note, the trailing '/' is required after {\tt $<$dcp-folder-name$>$}, while the '\textbackslash'
between the command line arguments can be omitted.
The '\textbackslash' is used to escape the newline characters, which make the command easier
to read in this tutorial. However, you can omit the '\textbackslash' and write the whole command
on the same command line, as well.
Also, if you have logged out (while the design synthesizes) you may need to {\tt export} the
\$CL\_DIR variable again, as the environment variables are reset each time you log back in.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Start AFI Creation}

Use the {\tt create-fpga-image} command to send the tarball file to Amazon for AFI creation.

\begin{verbatim}
    $ aws ec2 create-fpga-image \
        --region <region> \
        --name <afi-name> \
        --description <afi-description> \
        --input-storage-location Bucket=<dcp-bucket-name>,Key=<path-to-tarball> \
        --logs-storage-location Bucket=<logs-bucket-name>,Key=<path-to-logs> \
        [ --client-token <value> ] \
        [ --dry-run | --no-dry-run ]
\end{verbatim}


Note, {\tt $<$path-to-tarball$>$} is your {\tt $<$dcp-folder-name$>$/$<$tar-file-name$>$}, while {\tt $<$path-to-logs$>$}
is your {\tt $<$logs-folder-name$>$}.

The command line arguments in '[]' can be omitted, as well as the '\textbackslash' escape characters if the command is written
on the same line.

The output of this command includes two identifiers that refer to your AFI:

\begin{itemize}

\item \textbf{FPGA Image Identifier or AFI ID}: This is the main ID used to manage your AFI through 
the AWS CLI commands and AWS SDK APIs. This ID is regional, i.e., if an AFI is copied across multiple regions, 
it will have a different unique AFI ID in each region. An example AFI ID is afi-06d0ffc989feeea2a.

\item \textbf{Global FPGA Image Identifier or AGFI ID}: This is a global ID that is used to refer to an AFI from within an F1 instance. 
For example, to load or clear an AFI from an FPGA slot, you use the AGFI ID. Since the AGFI IDs is global (by design), 
it allows you to copy a combination of AFI/AMI to multiple regions, and they will work without requiring any extra setup. 
An example AGFI ID is agfi-0f0e045f919413242.

\end{itemize}

The {\tt create-fpga-image} command submits your Custom Logic to Amazon for approval. This usually takes about 20-30 minutes.
The {\tt describe-fpga-images} API allows you to check the AFI state during the background AFI generation process. 

You must provide the FPGA Image Identifier returned by {\tt create-fpga-image}:

\begin{verbatim}
    $ aws ec2 describe-fpga-images --fpga-image-ids afi-06d0ffc989feeea2a
\end{verbatim}

The AFI can only be loaded to an instance once the AFI generation completes and the AFI state is set to {\tt available}.

