\babel@toc {english}{}
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Prerequisites}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Amazon EC2 Account}{2}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}University-based Account}{2}{subsubsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}Individual Account}{3}{subsubsection.1.2.2}
\contentsline {section}{\numberline {2}Local Development Environment Setup}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Development Environment Setup on Linux Machine}{4}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Setup Linux and Xilinx FPGA Tools}{4}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Setup AWS Command Line Interface}{4}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}FPGA Development Tools Setup}{4}{subsection.2.2}
\contentsline {section}{\numberline {3}Creating Development VM Instance}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Create {\tt c4.4xlarge} Instance}{4}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Creating a Key Pair}{5}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Connecting to the Instance}{6}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}SSH'ing into the VM}{6}{subsubsection.3.1.3}
\contentsline {subsection}{\numberline {3.2}FPGA Development Tools Setup}{7}{subsection.3.2}
\contentsline {section}{\numberline {4}FPGA Development Tools Setup}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}Setting up AWS CLI and S3 Bucket to Enable AFI Creation}{7}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Setup AWS CLI Access}{7}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Create S3 Bucket}{8}{subsubsection.4.1.2}
\contentsline {subsection}{\numberline {4.2}Installing the HDK and Setting up the Environment}{8}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Get and Setup Amazon AWS FPGA Code}{9}{subsubsection.4.2.1}
\contentsline {section}{\numberline {5}Hello World Example}{9}{section.5}
\contentsline {subsection}{\numberline {5.1}Select Hello World Example for Synthesis}{9}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Start the Build Process for the Custom Logic}{10}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Monitoring Build Process}{10}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Submit the DCP Tarball to AWS to Create the AFI}{10}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}Start AFI Creation}{11}{subsection.5.5}
\contentsline {section}{\numberline {6}Creating F1 VM Instance with Access to FPGA}{12}{section.6}
\contentsline {subsection}{\numberline {6.1}Requesting Access to F1 Instances}{12}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Launch an F1 Instance}{13}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}SSH'ing into the VM}{13}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Setup the FPGA Management Tools and Configure AWS CLI}{13}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}Load the AGFI onto FPGA}{13}{subsection.6.5}
\contentsline {subsubsection}{\numberline {6.5.1}Pre-synthesized Hello World Example}{14}{subsubsection.6.5.1}
\contentsline {subsection}{\numberline {6.6}Validating the Design Using the CL Example Software}{14}{subsection.6.6}
\contentsline {subsection}{\numberline {6.7}Allow Other Users to Access Your AGFI}{15}{subsection.6.7}
\contentsline {subsection}{\numberline {6.8}Shutting Down VM when Done Working}{15}{subsection.6.8}
\contentsline {section}{\numberline {7}Acknowledgement}{16}{section.7}
