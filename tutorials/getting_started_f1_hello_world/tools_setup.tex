

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{FPGA Development Tools Setup}
\label{sec_tools_setup}

With either local machine or remote development VM setup, this section
describes the steps needed to setup FPGA development tools.
This section assumes that the user has setup a local machine or remote development VM
with Linux Ubuntu 18.04, has installed Xilinx FPGA tools version 2021.2, and has installed
AWS Command Line Interface.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Setting up AWS CLI and S3 Bucket to Enable AFI Creation}

As the developer, you are required to create an S3 bucket for the AFI (Amazon FPGA Image) generation. 
An S3 bucket is similar to a file stored on a remote server: The files are not stored inside the VM instance.
Rather, they are stored remotely in Amazon's cloud.
The bucket will contain a {\tt *.tar} file and logs which are generated from the AFI creation service.
To be able to create S3 buckets from the command line and to do other AWS related operations from the command line, the AWS CLI has to be first configured.

\subsubsection{Setup AWS CLI Access}

AWS CLI (Command Line Interface) tools need to know your credentials so that when you execute AWS CLI commands.
You can configure the AWS CLI using below command:

\begin{verbatim}
    $ aws configure
\end{verbatim}

You will need your security credentials.  Go to \url{https://console.aws.amazon.com/iam/home?#/security_credential}.
You may need to select {\tt Continue to Security Credentials} in a pop-up window.
Expand the {\tt Access keys (access key ID and secret access key)} tab.
Click {\tt Create New Access Key}. If you do not see an orange button to create a new key, try looking up {\tt Key Management Service} in
the search bar at the very top of the console window. The page should show an orange button to create a new key.

Make sure to download the access key file. You will not have access to the secret key after you finish the access key creation process.
The file will be a {\tt *.csv} (comma separated values file type) which can be opened with most spreadsheet programs or text editors.
Save the file for future access.

In the {\tt aws configure} command, you will need to enter your access key and the corresponding secret key.
Also, make sure to set the region to {\tt us-east-1} and select output format as {\tt json}.  
Note that the input text is case-sensitive, e.g.  {\tt us-east-1} and not {\tt US-East-1}
You can re-run the command if you make any mistakes.

\subsubsection{Create S3 Bucket}

This S3 bucket will be used by the AWS SDAccel scripts to upload your DCP (digital checkpoint)
to AWS for AFI generation.
Start by creating a bucket and a folder within your new bucket, details about the commands are below.

The first command creates an S3 bucket, which needs to have a unique bucket name among all other buckets in S3.
The file name cannot have uppercase letters or underscores.
Replace the $<$bucket-name$>$ with the actual bucket name you want to use.  Do not use $<$ or $>$ in the actual name.
The name can only use lowercase letters and dashes.
The name needs to be globally unique, so pre-pending the name with ``eeng428'' can help you
have a useful, but unique name for your bucket. It is useful to write down your new bucket name so you can reference
the files you store on it later.

\begin{verbatim}
    $ aws s3 mb s3://<bucket-name> --region us-east-1
    $ aws s3 mb s3://<bucket-name>/<dcp-folder-name>
    $ touch FILES_GO_HERE.txt
    $ aws s3 cp FILES_GO_HERE.txt s3://<bucket-name>/<dcp-folder-name>/
\end{verbatim}

The second command creates a folder within the bucket.
Replace the $<$dcp-folder-name$>$ with the actual folder name.
Again, do not use $<$ or $>$ in the actual name of the folder.

The third command simply creates a blank file in the local directory, not in bucket, yet.
The fourth command copies the blank file into the DCP folder in the bucket, which forces
the bucket's folders to be actually created.
Don't forget the trailing slash at end of last command.

The AFI creation process will also generate logs that will be placed into your S3 bucket. 
These logs can be used for debug if the AFI generation fails.
Thus, create a folder for your log files:

\begin{verbatim}
    $ aws s3 mb s3://<bucket-name>/<logs-folder-name>
    $ touch LOGS_FILES_GO_HERE.txt
    $ aws s3 cp LOGS_FILES_GO_HERE.txt s3://<bucket-name>/<logs-folder-name>/
\end{verbatim}

The first command creates a folder to keep your logs.
The second command creates a blank file again.
The third command copies blank file into the bucket and trigger creation of the folder on S3.

The same bucket can be re-used among projects or as you re-synthesize the design, e.g.
create different DCP sub-folders for each project.
Alternatively, you can create separate buckets for each project to keep things organized.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Installing the HDK and Setting up the Environment}

The hardware development kit (HDK) contains all the scripts and links to tools needed to synthesize your Custom Logic
designed in VHDL or Verilog into the digital checkpoint (DCP) that will be used to finally create the Amazon FPGA Image
(AFI) that can be used to configure the FPGA.

\subsubsection{Get and Setup Amazon AWS FPGA Code}

First, {\tt git clone} a copy of aws-fpga repository in the home directory for the VM.

\begin{verbatim}
    $ git clone https://github.com/aws/aws-fpga
\end{verbatim}

Next, enter the aws-fpga directory and source hdk\_set.sh.

\begin{verbatim}
    $ cd aws-fpga/
    $ source hdk_setup.sh
\end{verbatim}

Sourcing hdk\_setup.sh will set required environment variables that are used throughout the examples in the HDK.
Optionally, source software development kit (SDK) setup script to get access to scripts about AFI's 
created or scripts to get e-mails for AFI generation updates.

\begin{verbatim}
    $ source sdk_setup.sh
\end{verbatim}

You may need to source the HDK and SDK scripts each time you log into the VM.
