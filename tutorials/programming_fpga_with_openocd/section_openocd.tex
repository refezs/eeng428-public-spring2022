
\section{Programming FPGAs with OpenOCD}
\label{sec:openocd}

This tutorial gives a very brief introduction to the basics of OpenOCD: the Open On-Chip Debugger.
OpenOCD is a open-source tool created for debugging and programming embedded devices~\cite{openocd}.
OpenOCD software typically runs on a host computer, and communicates
via the target board or device through a debug adapter.
Most often, a JTAG adapter is used so that OpenOCD can communicate with the board.
FTDI is one common USB-JTAG adapter that is included on many FPGA boards so they can be connected to the computer
via common USB cable. The JTAG commands are sent from the host computer over USB to the USB-JTAG adapter
on the board, where the adapter extracts the JTAG commands and communicates them to the devices on the board.

Devices on the board are usually connected to JTAG as TAPs (Test Access Ports), each TAP is effectively a device such as FPGA chip,
EEPROM chip, etc. The TAPs are daisy-chained within and between chips and boards, meaning that
many TAPs are on a single JTAG connection.
Board manuals can be used to find addresses of different TAPs on a board.

\subsection{Starting OpenOCD Server}

OpenOCD typically starts up a ``server'' that is used by users, or scripts, to interact with the target device.
OpenOCD will first connect to the target device. Once connected, OpenOCD server can be used to execute commands
such as to program the FPGA with a bitstream. The commands to be executed can be issued as part of the {\tt -c} command line option,
discussed later. Alternatively, when OpenOCD starts up, it listens for Telnet connections on port {\tt 4444}. Once OpenOCD
is started, users can connect to the Telnet port and interactively issue commands, as discussed below as well.

\subsection{Connecting to an FPGA Board}

To connect to an FPGA board, or any other device, 
OpenOCD needs to be configured with the information about the debug adapter as well as the target device.
Many pre-existing configuration files are provided by OpenOCD for common types of adapters and devices. On a typical Linux machine
the configuration files are usually in {\tt /usr/share/openocd/scripts/} folder.

\subsubsection{Configuration Files}

OpenOCD will need information about the debug adapter or interface that is used, such as the FTDI. Example interface configuration
file for an FTDI adapter can be found in the {\tt ftdi} folder under {\tt interfaces} folder: {\tt /usr/share/openocd/scripts/interface/ftdi/digilent-hs1.cfg}.

OpenOCD also needs information about the device. For the 7-series devices the configuration can be found, e.g.,
in {\tt /usr/share/openocd/scripts/cpld/xilinx-xc7.cfg}.

\subsubsection{Establishing Connection to Target Device}

To establish a connection to a target device, OpenOCD needs to be invoked with {\tt -f} option to specify
scripts, i.e. configuration files, to be executed on startup,
and {\tt -c} to specify a list of any additional commands to be executed on startup.

To establish a connection to a 7-series Xilinx FPGA OpenOCD can be invoked as below:

\begin{verbatim}
    $ /usr/bin/openocd \
     -f /usr/share/openocd/scripts/interface/ftdi/digilent-hs1.cfg \ 
     -f /usr/share/openocd/scripts/cpld/xilinx-xc7.cfg \
     -c "ftdi_serial FTDI_SERIAL_NUMBER;" \
     -c "adapter_khz 1000; transport select jtag;"
\end{verbatim}

The above command includes configuration for FTDI adapter and generic 7-series device configuration,
in addition {\tt adapter\_khz 1000} is used to set JTAG adapter speed and {\tt transport select jtag} is used
to specify JTAG protocol to be used. Of course, the configuration files and additional commands may need
to be changed for different types of boards. Note that the {\tt FTDI\_SERIAL\_NUMBER} has to be replaced with the
actual serial number of the FTDI adapter, this is important when there are multiple
boards turned on at the same time and multiple FTDI adapters are connected to the host,
so that the OpenOCD connection is made to the correct adapter.

\subsection{Interacting with OpenOCD via Telnet}

When OpenOCD connects to a board, it automatically opens a Telnet connection on port {\tt 4444}.
Users can connect to Telnet and interact with OpenOCD by invoking the {\tt telnet} command from
a new terminal window, i.e. the OpenOCD will start and keep running in the first terminal window,
and users then need to open second terminal window from which they start the Telnet connection.

\begin{verbatim}
    $ telnet localhost 4444
\end{verbatim}

Once connected to Telnet, users can issue OpenOCD commands or any commands defined
by the {\tt proc} directives inside the {\tt *.cfg} configuration files. The configuration files can be viewed
with any text editor to find out what commands they define. To exit from Telnet at any point, type {\tt exit}.

\subsection{Programming FPGA with a Bitstream using OpenOCD}

To program the FPGA using a pre-compiled bitstream, the OpenOCD can be called with
the {\tt pld load} command. Below is an example for loading the AC701 FPGA board
with a bitstream:

\begin{verbatim}
    $ /usr/bin/openocd \
     -f /usr/share/openocd/scripts/interface/ftdi/digilent-hs1.cfg \ 
     -f /usr/share/openocd/scripts/cpld/xilinx-xc7.cfg \ 
     -c "ftdi_serial FTDI_SERIAL_NUMBER;" \
     -c "adapter_khz 1000; transport select jtag;" \
     -c "init; xc7_program xc7.tap; pld load 0 BITFILE_PATH; exit;"
\end{verbatim}

Note in the above command, {\tt BITFILE\_PATH} has to be replaced with the actual
path to the bitfile that should be loaded onto the FPGA.

\subsection{Closing OpenOCD}

If the OpenOCD is started with {\tt -c} option that specifies {\tt exit} command then OpenOCD
will exit immediately after executing all the commands.  Otherwise, OpenOCD stays active so, for example,
users can connect via Telnet. To close active OpenOCD server in the current terminal window, press {\tt Ctrl-C}.
This will terminate any Telnet sessions connected to OpenOCD if they have not been closed first.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Other Uses of OpenOCD}

OpenOCD can be used to interact with any devices or test access ports available via
JTAG connection to the host computer.

\subsection{Accessing XADC}

Xilinx 7-series FPGAs have an XADC which includes a dual 12-bit,
1 Mega sample per second (MSPS) analog-to-digital (ADC) converter and on-chip sensors for temperature and voltage%
\footnote{\url{https://www.xilinx.com/support/documentation/user_guides/ug480_7Series_XADC.pdf}}.
The XADC can be access from the host computer via  Joint Test Action Group (JTAG) port or from
within the FPGA via the dynamic reconfiguration port (DRP).

Since XADC can be accessed via JTAG, it is possible to read the sensor values on the host computer using OpenOCD.
Fortunately, OpenOCD comes with configuration files for accessing the XADC, they can be downloaded
from \url{https://github.com/openocd-org/openocd/blob/master/tcl/fpga/xilinx-xadc.cfg}. The {\tt xilinx-xadc.cfg} file defines a number
of useful commands for accessing the XADC sensors.

To connect to the FPGA and access the built-in XADC sensor data, the {\tt xilinx-xadc.cfg} needs to be added to the list of the configuration
files loaded on startup.  Further, the {\tt xadc\_report} command defined in the {\tt xilinx-xadc.cfg} can be used to print the temperature
and voltages.  The command requires one argument, which is the TAP that corresponds to the FPGA chip, the name of the TAP
in this case is {\tt xc7.tap} (this can be read from the OpenOCD messages printed when OpenOCD starts up):

\begin{verbatim}
    $ /usr/bin/openocd \
     -f /usr/share/openocd/scripts/interface/ftdi/digilent-hs1.cfg \ 
     -f /usr/share/openocd/scripts/cpld/xilinx-xc7.cfg \
     -f /home/$USER/xilinx-xadc.cfg \
     -c "ftdi_serial FTDI_SERIAL_NUMBER;" \
     -c "adapter_khz 1000; transport select jtag;" \
     -c "init; xadc_report xc7.tap; exit;"
\end{verbatim}

Note that the above command assumes the {\tt xilinx-xadc.cfg} file is in the user's home directory. Further, note that in most Linux terminals,
the {\tt \$USER} variable will be replaced with the user's actual user name.

The above command will start OpenOCD, print the temperature and voltage data, and exit. Users can also interactively get the data through Telnet.
To use Telnet, the commands {\tt init; xadc\_report xc7.tap; exit;} need to be removed (so that OpenOCD starts up, but then does nothing),
and once OpenOCD starts up, Telnet connection needs to be established in another terminal window, and the three commands need to be typed
manually. Of course, once one types exit, Telnet will exit, so to print the sensor data multiple times, one needs to execute the {\tt xadc\_report xc7.tap}
command as many times as wished before finally typing {\tt exit}.

\subsection{Accessing Device DNA}

Since device DNA can be accessed via JTAG, it is possible to read it on the host computer using OpenOCD.
Fortunately, OpenOCD comes with configuration files for accessing the device DNA, they can be downloaded
from \url{https://github.com/openocd-org/openocd/blob/master/tcl/fpga/xilinx-dna.cfg}. The {\tt xilinx-dna.cfg} file defines a number
of useful commands for reading and printing the device DNA.

Similar for accessing XADC, the device DNA requires adding the {\tt xilinx-dna.cfg} configuration file to the list of
files read by OpenOCD on startup, as well as the device DNA specific commands:

\begin{verbatim}
    $ /usr/bin/openocd \
     -f /usr/share/openocd/scripts/interface/ftdi/digilent-hs1.cfg \ 
     -f /usr/share/openocd/scripts/cpld/xilinx-xc7.cfg \
     -f /home/$USER/xilinx-dna.cfg \
     -c "ftdi_serial FTDI_SERIAL_NUMBER;" \
     -c "adapter_khz 1000; transport select jtag;" \
     -c "init; echo \"[xc7_get_dna xc7.tap]\"; exit;"
\end{verbatim}

Note here the {\tt xc7\_get\_dna xc7.tap} command returns the raw device DNA number and it is used inside the 
{\tt echo ""} command to actually print the value. The slashes before the double quotes are used to escape the quotes
so they are treated as part of the echo and not as part of the {\tt -c} command as a whole.