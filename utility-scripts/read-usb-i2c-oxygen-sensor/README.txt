# Install PyFTDI

Need to install required Python packages, not required on class server as they are
already installed:

$ pip3 install pyftdi

# Turning on the sensor

The O2 sensor is connected to the remote-controlled USB hub. It can be turned on/off using
the contrl_fpga.py script:

$ control_fpga.py -f CASLAB-O2-SENSOR-01 --enable

You can see the USB port in the usual ~/ports.json file, but it should not be needed
as the FTDI URL listed below is actually used to connect to the sensor.

# Sample usage

The read_o2_sensor.py script needs to be called with the FTDI URL of the sensor, comma separated
list of phone numbers to receive SMS, API key for the TextBelt service, threshold (if O2 concentration
is below threshold, an SMS message will be sent) and boolean flag to enable SMS sending. Use 'textbelt' string
as the API key to get 1 free SMS per day:

$ read_o2_sensor.py -f ftdi://ftdi:232h:FT0J25YH/1 -r 1231231234,4564564567 -a textbelt -t 25.0 --enablesms

# Running sensor monigoring in the background

You can run the script in background by using:

$ read_o2_sensor.py -f ftdi://ftdi:232h:FT0J25YH/1 -r 1231231234,4564564567 -a textbelt -t 25.0 --enablesms > /dev/null &

Later to find the Process ID of the script so you can kill it, use:

$ ps aux | grep read_o2_sensor | grep -v grep

# Finding out FTDI URL

Use the i2cscan.py to get a list of FTDI URLs on your system. The O2 sensor is connected to
C232HM USB adapter, so look for string '(C232HM-DDHSL-0)' in the output of the I2C scan:

$ i2cscan.py
