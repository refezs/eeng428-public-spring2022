#!/usr/bin/env python3

#
# Copyright (c) 2022, Jakub Szefer <jakub.szefer@yale.edu>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software 
# Foundation, either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with 
# this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

#
# References:
# 
# PyFtdi and it's I2C functions
# https://eblot.github.io/pyftdi/api/i2c.html#
#
# DFRobot_OxygenSensor Python library
# https://github.com/DFRobot/DFRobot_OxygenSensor/blob/master/python/raspberrypi/DFRobot_Oxygen.py
#
# The Gravity: I2C Oxygen Sensor, SKU SEN0322
# https://wiki.dfrobot.com/Gravity_I2C_Oxygen_Sensor_SKU_SEN0322
# https://www.dfrobot.com/product-2052.html
#
# C232HM USB 2.0 Hi-Speed to MPSSE Cable Datasheet, Page 9 has I2C connection specification
# https://www.ftdichip.com/Support/Documents/DataSheets/Cables/DS_C232HM_MPSSE_CABLE.pdf?page9
#

from pyftdi.i2c import I2cController
from os import sys
import requests
import time
import datetime
import argparse

# Send SMS message to a list of recipients notyfing them
# of the O2 sensor reading
def send_sms(recipients, o2sensordata, textbeltapikey, o2threshold):
    if o2sensordata < o2threshold:
        for number in recipients:
            msg = "CASLAB: O2 sensor reading is " + str(round(o2sensordata, 4))
            print('DEBUG -- ' + str(number))
            print('DEBUG -- ' + msg)
            resp = requests.post('https://textbelt.com/text', {
                'phone': number,
                'message': msg,
                'key': textbeltapikey,
            })
            print('DEBUG -- ')
            print(resp.json())
    else:
        print('DEBUG -- O2 threshold set at ' + str(round(o2threshold, 4)) + ' while current O2 reading is ' + str(round(o2sensordata, 4)))


# Collect O2 sensor data
def get_o2_sensor_data(ftdi):
    # Instantiate I2C controller and connect to the O2 sensor
    # Use i2cscan.py to find the correct URL for the FTDI-based sensor
    i2c = I2cController()
    i2c.configure(ftdi)

    # Specify target address for reading O2 data
    # https://wiki.dfrobot.com/Gravity_I2C_Oxygen_Sensor_SKU_SEN0322
    sensor = i2c.get_port(0x73)

    # Get prescale value, 1 byte from register 0x0A
    data  = sensor.read_from(0x0A, 1)
    data1 = sensor.read_from(0x0A, 1)

    prescale = float(data[0]) / float(1000)

    # Collect 20 measurements and evarage them out
    # O2 sensor data is in register 0x03 and uses 3 bytes
    o2_concentration = [0] * 20
    for num in range(0,20):
        data  = sensor.read_from(0x03, 3)
        data1 = sensor.read_from(0x03, 3)

        o2_concentration[num] = prescale * (float(data[0]) + float(data[1]) / 10.0 + float(data[2]) / 100.0)

    temp = float(0)
    for num in range(0,20):
        temp += o2_concentration[num]

    avr_o2_concentration = temp / float(20)

    # Print data
    print('DEBUG -- ' + 'Day and time: ' + str(datetime.datetime.now()))
    print('DEBUG -- ' + 'Prescale: ' + str(prescale))
    print('DEBUG -- ' + 'O2 concentration: ' + str(round(avr_o2_concentration, 4)))

    # Return data
    return avr_o2_concentration


# Main script
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Monitor I2C-based Oxygen sensor.')
    parser.add_argument("-f", "--ftdi", help='FTDI URL identifier for O2 sensor', required=True)
    parser.add_argument("-r", "--recipients", help='List of recipients\'s phone numbers', required=True)
    parser.add_argument("-a", "--apikey", help='Textbelt API key, use \'textbelt\' for 1 free SMS per day', required=True)
    parser.add_argument("-t", "--threshold", help='O2 concentration threshold for alerts', required=True)
    parser.add_argument("-e", "--enablesms", action='store_true', help='Enable sending SMS', required=True)
    args = parser.parse_args()

    # FTDI URL of the O2 sensor, see i2cscan.py for examples
    print('DEBUG -- args.ftdi        = ' + args.ftdi)
    print('DEBUG -- args.recipients  = ' + args.recipients)
    print('DEBUG -- args.aptikey     = ' + args.apikey)
    print('DEBUG -- args.threshold   = ' + args.threshold)
    print('DEBUG -- args.enablesms   = ' + str(args.enablesms))

    # Loop forever monitoring O2 sensor data
    print('DEBUG -- starting to monitor O2 sensor data')
    enable_sms = args.enablesms
    o2threshold = float(args.threshold)
    recipients = args.recipients.split(',')
    while True:
        o2sensordata = get_o2_sensor_data(args.ftdi)
        if enable_sms:
            send_sms(recipients, o2sensordata, args.apikey, o2threshold)

        if o2sensordata < o2threshold and enable_sms == True:
            print('DEBUG -- Alert message was sent, stopping future SMSes')
            enable_sms = False

        time.sleep(5)
